package com.example.tipcalculatoractivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Click listener for calculate button
        button.setOnClickListener{ handleClick() }
    }

    enum class TipPercentage { tenP, twentyP, thirtyP }

    private fun handleClick() {
        try {
            // Get entered amount
            val amountWithoutTip = userInput.editableText.toString()

            // Get tip percentage
            val tipPercentage = when (radioGroup.checkedRadioButtonId) {
                R.id.tenPercent -> TipPercentage.tenP
                R.id.twentyPercent -> TipPercentage.twentyP
                R.id.thirtyPercent -> TipPercentage.thirtyP
                else -> throw Exception()
            }

            // Convert user input to double
            val amountWithoutTipDouble = amountWithoutTip.toDouble()

            // Calculate tip and total with tip
            val resultAmount = calcTotalAmount(amountWithoutTipDouble, tipPercentage)
            val resultTip = calcTipAmount(amountWithoutTipDouble, tipPercentage)

            resultText.text = "Your total amount is " + resultAmount + " and your tip amount is " + resultTip
        }
        catch(e: Exception) {
            // Error message
             resultText.text = "Error"
        }

    }

    // Calculates tip and total with tip
    fun calcTotalAmount(amount: Double, tip: TipPercentage) {
        val total: Double

         return when(tip) {
              TipPercentage.tenP -> {
                total = amount + (amount * .10)
            }

             TipPercentage.twentyP -> {
                 total = amount + (amount * .20)
             }

             TipPercentage.thirtyP -> {
                 total = amount + (amount * .30)
             }
        }
    }

    fun calcTipAmount(amount: Double, tip: TipPercentage){
        val tipP: Double

        return when(tip) {
            TipPercentage.tenP -> {
                tipP = amount * (.10)
            }

            TipPercentage.twentyP -> {
                tipP = amount + (.20)
            }

            TipPercentage.thirtyP -> {
                tipP = amount + (.30)
            }
        }
    }
}
